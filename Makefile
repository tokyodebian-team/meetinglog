SOURCE:=$(wildcard *.xml)
PDFFILES:=$(SOURCE:%.xml=%.pdf)
TXTFILES:=$(SOURCE:%.xml=%.txt)

all: pdf txt

pdf: $(PDFFILES)

txt: $(TXTFILES)

%.pdf: %.xml
	-rm -rf work/
	-mkdir work/
	LC_ALL=ja_JP.UTF-8 ./meeting-to-pdf.cc < $< 
	cp work/meetinglog.pdf $@

%.txt: %.xml
	LC_ALL=ja_JP.UTF-8 ./meeting.cc < $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	LC_ALL=ja_JP.UTF-8 ./meeting-to-html.cc < $< > $@.tmp
	mv $@.tmp $@

clean:
	-rm -r work
	-mkdir work
	-rm *~

check-syntax:
	# for flymake
	$(CXX) -c -O2 -Wall $(CHK_SOURCES)  -o/dev/null

check:
	set -e; for A in *_test.sh; do bash $$A; done

.PHONY:	check-syntax check clean pdf txt

# Do not allow parallel execution since the rule contains rules with 'work'
.NOTPARALLEL: