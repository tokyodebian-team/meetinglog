<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet>
<!-- 
An experimental XSLT for replacing functionality of meeting-to-pdf.cc, convert XML to .tex

Tested with:
xsltproc latex.xsl 20110526.xml > work/a.tex && platex work/a.tex

TODO: escape chars like # and & more reasonably, right now it's changed to '-'.
 -->
  <xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="text" encoding="iso-2022-jp"/>
  <xsl:template match="/">
    <xsl:text>\documentclass[mingoth,a4paper]{jsarticle}
\usepackage[dvipdfm]{graphicx}
\usepackage{fancybox}
\usepackage{longtable}
\usepackage{ascmac}
\usepackage[dvipdfm]{hyperref}
\usepackage{url}
\usepackage[dvipdfm]{color}
\AtBeginDvi{\special{pdf:tounicode EUC-UCS2}}


\makeatletter
\renewcommand\tableofcontents{%
  \@starttoc{toc}}
\makeatother

\newcommand{\discussion}[4]{%
 \section{#1}
 \begin{tabular}{|p{20em}|p{20em}|}
 \hline
 \hline
 \multicolumn{2}{|c|}{目的}\\
 \hline
 \multicolumn{2}{|p{40em}|}{#2}\\
 \hline
 経緯 &amp; 議論 \\
 \hline
 #3 &amp; #4 \\
 \hline
 \hline
 \end{tabular}
}

\begin{document}

\begin{tabular}{|p{10em}|p{30em}|}
\hline
\hline
 \multicolumn{2}{|c|}{Debian JP 定例会議}\\
\hline
 開催場所: &amp; IRC $\sharp{}$debianjp at osdn.debian.or.j\\
 &amp;   漢字コード: utf-8\\
\hline
 会議名 &amp; </xsl:text><xsl:value-of select="meetinglog/head/title" /><xsl:text> \\
開催時間 &amp; </xsl:text><xsl:value-of select="meetinglog/head/date"/><xsl:text> \\
出席者: &amp; \\
</xsl:text>
    <xsl:for-each select="meetinglog/head/member">
      <xsl:text>  </xsl:text>&amp;<xsl:value-of select="."/>\\<xsl:text>
</xsl:text>
    </xsl:for-each>
<xsl:text>
\hline
 議案 &amp;
\begin{minipage}{1\hsize}
\vspace{1zw}
{\small
\tableofcontents
}
\vspace{1zw}
\end{minipage}
 \\
\hline
\hline
\end{tabular}</xsl:text>

    <xsl:for-each select="meetinglog/body">
      \discussion{<xsl:value-of select="./title" />}{<xsl:value-of select="./aim" />}{<xsl:value-of select="translate(./previous,'#&amp;','--')" disable-output-escaping="yes" />}{<xsl:value-of select="translate(./discussed,'#&amp;','--')" disable-output-escaping="yes" />}
    </xsl:for-each>
    \end{document}
  </xsl:template>
</xsl:stylesheet>
