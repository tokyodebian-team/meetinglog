/*BINFMTCXX: $(pkg-config libxml++-2.6 --cflags --libs)
  Load document in DOM, and play with it.
 */

#include <iostream>
#include <vector>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/foreach.hpp>
#include <glibmm.h>
//#include <libxml++/parsers/domparser.h>
//#include <libxml++/document.h>
#include <libxml++/libxml++.h>


using namespace std;
using namespace xmlpp;
using namespace boost::lambda;

int main(int argc, char** argv) {
  if (argc != 2) {
    cerr << "Command line flags: " << endl <<
      "   XML file name" << endl;
    return 1;
  }
  DomParser d(argv[1]);
  if(!d) {
    cerr << "DOM not parsed" << endl;
    return 1;
  }
  Element* e = d.get_document()->get_root_node();
  cout << e->get_name() << endl;
  Node::NodeList nl=e->get_children();
  for_each(nl.begin(), nl.end(),
	   std::cout << " " << bind(&Node::get_name, _1));

  // code to parse Gitweb-generated RSS.
  NodeSet ns=e->find("channel/item");
  BOOST_FOREACH(Node* entry, ns) {
    std::cout << "   " << entry->get_name() << endl;
    std::cout << "    " << dynamic_cast<TextNode*>(entry->find("title/text()").at(0))->get_content() << endl;
    std::cout << "    " << dynamic_cast<TextNode*>(entry->find("author/text()").at(0))->get_content() << endl;
    std::cout << "    " << dynamic_cast<TextNode*>(entry->find("pubDate/text()").at(0))->get_content() << endl;
    //std::cout << "    " << dynamic_cast<TextNode*>(entry->find("description/text()").at(0))->get_content() << endl;
  }
  return 0;
}
