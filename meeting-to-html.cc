/*BINFMTCXX: -ldancer-xml -lboost_regex-mt

Translate XML to HTML

Get XML file as stdin, and output to stdout.

With gijiroku-reload.js, will reload parts of HTML file from modified
HTML.

 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ctime>
extern "C" {
#include <dancer-xml.h>
}

#include <boost/regex.hpp>

std::string text_escaped(std::string a)
{
  const boost::regex r1("\n");
  const std::string t1("<br>");

  const boost::regex r2("https?://[^ 	\n]*");
  const std::string t2("<a href=\"$0\">$0</a>");

  const boost::regex r3("<");
  const std::string t3("&lt;");

  return boost::regex_replace
    (boost::regex_replace
    (boost::regex_replace
     (a, r3, t3, boost::format_all),
     r2, t2, boost::format_all),
     r1, t1, boost::format_all);
}

int main()
{
  dxml_element *xml, *member, *body;
  xml=dxml_read_xml(stdin);
  int i;

  std::cout <<
    //"Content-type: text/html\n\n"
    "<html>\n"
    "  <head>\n"
    "    <link rel=\"stylesheet\" type=\"text/css\" href=\"meeting.css\">\n"
    "    <script type=\"text/javascript\" src=\"./gijiroku-reload.js\"></script>\n"
    "    <meta HTTP-EQUIV=\"content-type\" CONTENT=\"text/html; charset=UTF-8\">\n"
    "    <meta name=\"robots\" content=\"noindex,nofollow\">\n"
    "    <title>"<< dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/title")
	    << "</title>\n"
    "  </head>\n"
    "  <body>\n"

    "    <h1>"<< dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/title")
	    << "</h1>\n"
    "    <table>\n"
    "      <tbody>\n"
    "	<tr>\n"
    "	  <th>開催場所</th>\n"
    "	  <td>IRC #debianjp at osdn.debian.or.jp <br>漢字コード: utf-8</td>\n"
    "	</tr>\n"

    "	<tr>\n"
    "	  <th>会議名</th>\n"
    "	  <td>"<<dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/title")
	    << "</td>\n"
    "	</tr>\n"

    "	<tr>\n"
    "	  <th>開催時間</th>\n"
    "	  <td>"<<dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/date")
	    << "</td>\n"
    "	</tr>\n"
    "	<tr>\n"
    "	  <th>参加者</th>\n"
    "	  <td>";

  member=dxml_get_element_bysimplepath(xml, "meetinglog/head/member");
  while(member)
    {
      std::cout << member->child->element_name << " ";
      member=member->next;
    }

  std::cout
	    << "</td>\n"
    "	</tr>\n";


  for (body=dxml_get_element_bysimplepath(xml, "meetinglog/body"), i=1;
       body;
       body=body->next, i++)
    {
      std::cout <<
	"	<tr>\n"
	"	  <th><a href=\"#gian"<< i <<"\">議案"<< i <<"</a></th>\n"
	"	  <td class=bodytitle><a href=\"#gian"<< i <<"\">"
	   << dxml_get_PCDATA_bysimplepath(body, "body/title")
	   << "</a></td>\n"
	"	</tr>\n";
    }

  std::cout <<
    "      </tbody>\n"
    "    </table>\n";

  for(i=1, body=dxml_get_element_bysimplepath(xml, "meetinglog/body");
      body; body=body->next, i++)
    {
      std::string aim(dxml_get_PCDATA_bysimplepath(body, "body/aim")?:"");
      std::string previous(dxml_get_PCDATA_bysimplepath(body, "body/previous")?:"");
      std::string discussed(dxml_get_PCDATA_bysimplepath(body, "body/discussed")?:"");

      std::cout <<
	"    <h2> <a href=\"#gian" << i << "\" name=\"gian" << i << "\">"
		<< "議案" << i << ". "
		<< (dxml_get_PCDATA_bysimplepath(body, "body/title")?:"")
		<< "</a></h2>\n"
	"    <table>\n"
	"      <tbody>\n"
	"	<tr>\n"
	"	<th colspan=2>目的</th>\n"
	"	</tr>\n"
	"	<tr>\n"
	"	<td colspan=2>" << text_escaped(aim) << "</td>\n"
	"	</tr>\n"
	"	<tr>\n"
	"	<th >経緯</th>\n"
	"	<th >議論</th>\n"
	"	</tr>\n"
	"	<tr>\n"
	"	<td id=\"previous" << i << "\">"
				<< text_escaped(previous) <<"</td>\n"
	"	<td id=\"discussed" << i << "\">"
				<< text_escaped(discussed) <<"</td>\n"
	"	</tr>\n"
	"      </tbody>\n"
	"    </table>\n";
    }

  time_t     t;
  struct tm  *local;
  time(&t);
  local = localtime(&t);

  std::cout <<
    "    <p><i id=\"lastupdated\"> 最終更新: " << asctime(local) <<
    "</i></p>\n"
    "  </body>\n"
    "</html>\n\n";
}
