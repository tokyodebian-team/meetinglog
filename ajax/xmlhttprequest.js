
var kSecMsecs = 1000
var timer_interval = 1 * kSecMsecs;

var my_timer = function() {
    var xml = new XMLHttpRequest();
    xml.open('GET', window.location + '#test', true);
    xml.onreadystatechange = function() {
	if (xml.readyState == 4) {
	    on_xml_loaded(xml);
	}
    };
    xml.overrideMimeType('text/xml');
    xml.send('');
    setTimeout(my_timer, timer_interval);
};

var counter = 0;


// output debug string to last-updated field.
var debug_output = function(s) {
    document.getElementById('debug').innerHTML = s;
}

var on_xml_loaded = function(xml) {
    // get the XMLHTTPRequest response text into res value.
    var res=xml.responseText;
    debug_output("debug" + counter);

    // test that first line match is possible -- OK
    var m = res.match(/PUBLIC(.*)/);
    debug_output(m[1]);

    // test that other line match is possible -- OK
    var m = res.match(/<he(ad)>/);
    debug_output(m[1]);

    // test that multi-line match is possible. -- OK
    m = res.match(/<title>([\s\S]*)<\/title>/);
    debug_output(m[1]);

    // test giji extraction is possible -- this part requires utf-8.
    //m = res.match(/<td id="previous1">([\s\S]*)<\/td>/);
    //debug_output(m[1]);

    // test giji extraction is possible -- this part requires utf-8. -- OK
    var re = new RegExp("<td id=\"previous" + 1 + "\">(.*)<\/td>");
    m = res.match(re);
    debug_output(m[1]);
    

    counter++;
};

my_timer();
