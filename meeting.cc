/*BINFMTCXX: -ldancer-xml

Translate XML to txt

only works for UTF-8 locale.

 */

#define _XOPEN_SOURCE
#include <iostream>

#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <cstring>
#include <locale.h>
#include <wchar.h>

extern "C" {
#include <dancer-xml.h>
}

using namespace std;

/* output kanji string to stdout with filled paragraph */
void print_fill_paragraph(const char*s)
{
  int len;
  char *b=new char[strlen(s)+1];
  int displaywidth=0;
  wchar_t wc;

  while (*s)
    {
      mbstate_t mbstate1;
      mbstate_t mbstate2;

      memset (&mbstate1, 0, sizeof(mbstate_t));
      memset (&mbstate2, 0, sizeof(mbstate_t));
      strcpy(b,s);
      len=mbrlen(b, strlen(b), &mbstate1);
      b[len]=0;
      mbrtowc(&wc, b, len, &mbstate2);
      displaywidth+=wcwidth(wc);

      if (*b=='\n')
	{
	  displaywidth=0;
	}
      if (displaywidth > 70)
	{
	  /* 行末禁則処理、これらの文字では改行しない。
	     日本語の句読点、
	     英語文字のアルファベット、
	     また、URLを途中で改行しないために、一部の文字では改行を挿入しない。
	   */
	  if (wcschr(L"？。、.,「"
		     L"-@/~:", wc) ||
	      isalnum(*b))
	    {
	    }
	  else
	    {
	      cout << endl;
	      displaywidth=0;
	    }
	}
      cout << b;
      s+=len;
    }
  delete(b);
}

int main()
{
  dxml_element *xml, *member, *body;
  xml=dxml_read_xml(stdin);

  setlocale(LC_ALL, "ja_JP.UTF-8");

#define SEPARATOR "-----------------------------------------------------------------------\n"
  cout <<
    SEPARATOR <<
    "概要" <<  endl <<
    SEPARATOR <<
    "開催場所: IRC #debianjp @ osdn.debian.or.jp" << endl <<
    "	   漢字コード: utf-8" << endl <<
    "会議名： " << dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/title") << endl <<
    "開催時間： " << dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/date") << endl <<
    "出席者: " << endl;
  member=dxml_get_element_bysimplepath(xml, "meetinglog/head/member");
  while(member)
    {
      cout << "  " << member->child->element_name << endl;
      member=member->next;
    }

  cout << endl <<
    SEPARATOR <<
    "議題" << endl <<
    SEPARATOR;

  int i=0;
  for (body=dxml_get_element_bysimplepath(xml, "meetinglog/body");
       body;
       body=body->next)
    {
      cout << "  " << ++i << ". "  <<
	dxml_get_PCDATA_bysimplepath(body, "body/title") <<
	endl;
    }


  for (i=0, body=dxml_get_element_bysimplepath(xml, "meetinglog/body");
       body;
       body=body->next)
    {
      char *s;

      cout << endl <<
	SEPARATOR <<
	"[" <<
	++i << ". " <<
	dxml_get_PCDATA_bysimplepath(body, "body/title") <<
	"]" << endl <<
	SEPARATOR << endl <<
	"目的: " << endl;
      print_fill_paragraph(dxml_get_PCDATA_bysimplepath(body, "body/aim"));
      cout << endl << endl ;

      if((s=dxml_get_PCDATA_bysimplepath(body, "body/previous")))
	{
	  cout << "前回までの経緯:" << endl ;
	  print_fill_paragraph(s);
	  cout << endl << endl ;
  	}
      if((s=dxml_get_PCDATA_bysimplepath(body, "body/discussed")))
	{
	  cout << "議論:" << endl;
	  print_fill_paragraph(s);
	  cout << endl << endl ;
	}
    }

  cout << endl << endl
       << "To obtain the source:" << endl
       << "git clone ssh://git.debian.org/git/tokyodebian/meetinglog.git"
       << endl;

  return 0;
}
