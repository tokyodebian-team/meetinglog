<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet>
<!-- 

An experimental XSLT for replacing functionality of meeting-to-html.cc

Example invocation:
xsltproc html.xsl 20110526.xml > test.html
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="html" />
  <xsl:template match="/">
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="./meeting.css" />
	<script type="text/javascript" src="./gijiroku-reload.js" >
	  <!-- I don't understand but chrome does not like empty script/ -->
	  <xsl:comment>this is a comment.</xsl:comment>
	</script>
	<meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex,nofollow" />
	<title><xsl:value-of select="meetinglog/head/title"/></title>
      </head>
      <body>
	<h1><xsl:value-of select="meetinglog/head/title"/></h1>
	<table>
	  <tbody>
	    <tr>
	      <th>開催場所</th>
	      <td>IRC #debianjp at osdn.debian.or.jp <br/>漢字コード: utf-8</td>
	    </tr>

	    <tr>
	      <th>会議名</th>
	      <td><xsl:value-of select="meetinglog/head/title"/></td>
	    </tr>
	    <tr>
	      <th>開催時間</th>
	      <td><xsl:value-of select="meetinglog/head/date"/></td>
	    </tr>
	    <tr>
	      <th>参加者</th>
	      <td>
		<xsl:for-each select="meetinglog/head/member">
		  <xsl:value-of select="."/><xsl:text> </xsl:text>
		</xsl:for-each>
	      </td>
	    </tr>

	    <xsl:for-each select="meetinglog/body">
	      <tr>
		<th>
		  <xsl:element name="a">
		    <xsl:attribute name="href">#gian<xsl:value-of select="position()" /></xsl:attribute>
		  </xsl:element>
		  議案<xsl:value-of select="position()" />
		</th>
		<td class="bodytitle">
		  <xsl:value-of select="./title" />
		</td>
	      </tr>
	    </xsl:for-each>
	  </tbody>
	</table>

	<xsl:for-each select="meetinglog/body">
	  <h2><xsl:element name="a">
	      <xsl:attribute name="href">#gian<xsl:value-of select="position()" /></xsl:attribute>
	      <xsl:attribute name="name">gian<xsl:value-of select="position()" /></xsl:attribute>
	      議案<xsl:value-of select="position()" />.<xsl:text> </xsl:text><xsl:value-of select="./title"/>
	    </xsl:element>
	  </h2>
	  <table>
	    <tbody>
	      <tr><th colspan="2">目的</th></tr>
	      <tr><td colspan="2">
		  <xsl:value-of select="./aim" />
		</td></tr>
	      <tr><th>経緯</th><th>議論</th></tr>
	      <tr>
		<xsl:element name="td">
		  <xsl:attribute name="id">previous<xsl:value-of select="position()" /></xsl:attribute>
		  <xsl:value-of select="./previous" />
		</xsl:element>
		<xsl:element name="td">
		  <xsl:attribute name="id">discussed<xsl:value-of select="position()" /></xsl:attribute>
		  <xsl:value-of select="./discussed" />
		</xsl:element>
	      </tr>
	    </tbody>
	  </table>
	</xsl:for-each>
	<p><i id="lastupdated"> 最終更新: Thu Jun  2 07:12:10 2011</i></p>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
