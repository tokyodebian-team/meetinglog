/*BINFMTCXX:

watch a file, and as it changes, compile/send it to server

The remote place needs meeting.css file and gijiroku-reload.js

scp meeting.css gijiroku-reload.js netfort.gr.jp:public_html/tmp/
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <sched.h>
#include <iostream>
#include <sstream>

using namespace std;

#define IN_MY_EVENTS ((IN_ALL_EVENTS)& ~( IN_ACCESS | IN_OPEN | IN_CLOSE_NOWRITE ))

void do_action(const char* source_filename, const char* target_server, const char* target_filename)
{
  stringstream ss;
  ss << "./meeting-to-html.cc < " << source_filename
     << " | ssh " << target_server
     << " \" cat > " << target_filename << "- && "
     << "mv " << target_filename << "- " << target_filename << "\"";
  cout << "starting html generation to remote \n";
  system(ss.str().c_str());
  cout << "  done remote html send\n";
}

int main(int argc, char** argv)
{
  struct inotify_event buf[1024];
  int fd = inotify_init ();
  int wd = inotify_add_watch (fd, argv[1], IN_MY_EVENTS);
  size_t len;
  int i;

  if (fd<0)
    perror("inotify");

  if (argc != 4)
    {
      cerr << "Usage:\n\t" << argv[0] << " xml-filename ssh-user@server remote-html-path" << endl;
      exit (1);
    }

  do_action(argv[1], argv[2], argv[3]);

  /* loop to find changes to the watched file */
  while ((len = read (fd, buf, sizeof(buf)))) {
    cout << "inotify event with " << len << " bytes" << endl;
    for (i=0; i * sizeof(struct inotify_event) < len; ++i) {
      cout << "  mask:" << buf[i].mask << endl;
      if (buf[i].mask & IN_MOVE_SELF) {
	inotify_rm_watch(fd, wd);
	sched_yield();
	wd = inotify_add_watch(fd, argv[1], IN_MY_EVENTS);
	if (wd<0)
	  perror("add_watch");
      }
    }
    sleep(1);
    do_action(argv[1], argv[2], argv[3]);
  }
  return 0;
}
