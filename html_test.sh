#!/bin/bash
set -e

export LC_ALL=ja_JP.UTF-8
diff -uw \
    <(xsltproc html.xsl testdata/basic.xml | w3m -dump -T text/html | grep -v '最終更新') \
    <(./meeting-to-html.cc < testdata/basic.xml | w3m -dump -T text/html | grep -v '最終更新')

