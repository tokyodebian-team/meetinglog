<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY space  "<xsl:text xmlns:xsl='http://www.w3.org/1999/XSL/Transform'> </xsl:text>">
<!ENTITY indent "<xsl:text xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>  </xsl:text>">
<!ENTITY cr     "<xsl:text xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
</xsl:text>">]>
<!-- 
An experimental XSLT for replacing functionality of meeting.cc, convert XML to .txt

Tested with:
xsltproc txt.xsl 20110526.xml 

LC_ALL=ja_JP.UTF-8 diff -u <( ./meeting.cc < 20110526.xml ) <(xsltproc txt.xsl 20110526.xml)

TODO: filling is not done (not sure if I can do that in XSLT)
 -->
  <xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="text" />
  <xsl:template match="/">
    <xsl:text>-----------------------------------------------------------------------
概要
-----------------------------------------------------------------------
開催場所: IRC #debianjp @ osdn.debian.or.jp
	   漢字コード: utf-8
会議名： </xsl:text><xsl:value-of select="meetinglog/head/title" /><xsl:text>
開催時間： </xsl:text><xsl:value-of select="meetinglog/head/date"/><xsl:text>
出席者: 
</xsl:text>
    <xsl:for-each select="meetinglog/head/member">
      &indent;<xsl:value-of select="."/>&cr;
    </xsl:for-each>
-----------------------------------------------------------------------
議題
-----------------------------------------------------------------------&cr;
<xsl:for-each select="meetinglog/body">
      &indent;<xsl:value-of select="position()" />.&space;<xsl:value-of select="./title" />&cr;
    </xsl:for-each>
    <xsl:for-each select="meetinglog/body">
-----------------------------------------------------------------------
[<xsl:value-of select="position()" />.&space;<xsl:value-of select="./title" />]
-----------------------------------------------------------------------

目的: &cr;<xsl:value-of select="./aim" />&cr;
&cr;<xsl:if test="string-length(normalize-space(./previous))>1">前回までの経緯:&cr;<xsl:value-of select="./previous" disable-output-escaping="yes" />&cr;&cr;</xsl:if>
<xsl:if test="string-length(normalize-space(./discussed))>1">議論:&cr;<xsl:value-of select="./discussed" disable-output-escaping="yes" />&cr;&cr;&cr;
</xsl:if>
</xsl:for-each>
To obtain the source:
git clone ssh://git.debian.org/git/tokyodebian/meetinglog.git
</xsl:template>
</xsl:stylesheet>
