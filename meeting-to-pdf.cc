/*BINFMTCXX: -ldancer-xml -lboost_regex-mt

Translate XML to pdf

only works for UTF-8 locale

 */


#include <cstdio>
#include <cstdlib>
extern "C" {
#include <dancer-xml.h>
}

#include <boost/regex.hpp>

std::string text_escaped(std::string a)
{
  const boost::regex r1("https?://[^ \n]*");
  const std::string t1("\\\\url{$0}");

  const boost::regex r2("#");
  const std::string t2("\\\\#");

  const boost::regex r3("%");
  const std::string t3("\\\\%");

  const boost::regex r4("_");
  const std::string t4("\\\\_");

  const boost::regex r5("&");
  const std::string t5("\\\\&");

  return boost::regex_replace
    (boost::regex_replace
     (boost::regex_replace
      (boost::regex_replace
       (boost::regex_replace
	(a, r5, t5, boost::format_all),
	r4, t4, boost::format_all),
       r3, t3, boost::format_all),
      r2, t2, boost::format_all),
     r1, t1, boost::format_all);
}

int main()
{
  dxml_element *xml, *member, *body;
  xml=dxml_read_xml(stdin);
  FILE* tex=fopen("work/meetinglog.tex.utf", "w");

  fprintf(tex,
"\\documentclass[mingoth,a4paper]{jsarticle}\n"
"\\usepackage[dvipdfm]{graphicx}\n"
"\\usepackage{fancybox}\n"
"\\usepackage{longtable}\n"
"\\usepackage{ascmac}\n"
"\\usepackage[dvipdfm]{hyperref}\n"
"\\usepackage{url}\n"
"\\usepackage[dvipdfm]{color}\n"
"\\AtBeginDvi{\\special{pdf:tounicode EUC-UCS2}}\n"
"\n"
"\n"
"\\makeatletter\n"
"\\renewcommand\\tableofcontents{%%\n"
"  \\@starttoc{toc}}\n"
"\\makeatother\n"
"\n"
"\\newcommand{\\discussion}[4]{%%\n"
" \\section{#1}\n"
" \\begin{tabular}{|p{20em}|p{20em}|}\n"
" \\hline\n"
" \\hline\n"
" \\multicolumn{2}{|c|}{目的}\\\\\n"
" \\hline\n"
" \\multicolumn{2}{|p{40em}|}{#2}\\\\\n"
" \\hline\n"
" 経緯 & 議論 \\\\\n"
" \\hline\n"
" #3 & #4 \\\\\n"
" \\hline\n"
" \\hline\n"
" \\end{tabular}\n"
"}\n"
"\n"
"\\begin{document}\n"
"\n"
"\\begin{tabular}{|p{10em}|p{30em}|}\n"
"\\hline\n"
"\\hline\n"
" \\multicolumn{2}{|c|}{%s}\\\\\n"
"\\hline\n"
" 開催場所: &IRC $\\sharp{}$debianjp at osdn.debian.or.jp\\\\\n"
" &   漢字コード: utf-8\\\\\n"
"\\hline\n"
" 会議名 & %s\\\\\n"
"\\hline\n"
" 開催時間 & %s\\\\\n"
"\\hline\n"
	  " 出席者 & \\\\\n",
	  dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/title"),
	  dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/title"),
	  dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/date"));

  member=dxml_get_element_bysimplepath(xml, "meetinglog/head/member");
  while(member)
    {
      fprintf(tex," & %s\\\\\n",
	      text_escaped(member->child->element_name).c_str());
      member=member->next;
    }

  fprintf(tex,
"\\hline\n"
" 議案 & \n"
"\\begin{minipage}{1\\hsize}\n"
"\\vspace{1zw}\n"
"{\\small\n"
"\\tableofcontents\n"
"}\n"
"\\vspace{1zw}\n"
"\\end{minipage}\n"
" \\\\\n"
"\\hline\n"
"\\hline\n"
"\\end{tabular}\n");


  body=dxml_get_element_bysimplepath(xml, "meetinglog/body");
  while(body)
    {
      std::string aim(dxml_get_PCDATA_bysimplepath(body, "body/aim")?:"");
      std::string previous(dxml_get_PCDATA_bysimplepath(body, "body/previous")?:"");
      std::string discussed(dxml_get_PCDATA_bysimplepath(body, "body/discussed")?:"");


      fprintf(tex,"\\discussion{%s}{%s}{%s}{%s}\n",
	      dxml_get_PCDATA_bysimplepath(body, "body/title"),
	      text_escaped(aim).c_str(),
	      text_escaped(previous).c_str(),
	      text_escaped(discussed).c_str());
      body=body->next;
    }
  fprintf(tex,
	  "\n\n\\vspace{1cm}\\hfill{}以 上\n\n\\end{document}\n");


  fclose(tex);
  return system("cd work && "
		"iconv -c -f utf-8 -t iso-2022-jp < meetinglog.tex.utf > meetinglog.tex && "
		"rm meetinglog.tex.utf && "
		"LC_ALL=ja_JP.eucJP locale &&"
		"env LC_ALL=ja_JP.eucJP platex meetinglog.tex && "
		"env LC_ALL=ja_JP.eucJP platex meetinglog.tex && "
		"env LC_ALL=ja_JP.eucJP platex meetinglog.tex && "
		"LC_ALL=ja_JP.EUC-JP dvipdfmx meetinglog.dvi");
}
