/*BINFMTCXX: -ldancer-xml

Try collecting members who attended the meetings.

./shukei-shusseki.cc *.xml | iconv -f utf-8 -t euc-jp  | cut -d, -f1  | sort| uniq -c  | sort -rn

 */
#include <iostream>
#include <cstdio>
#include <cstdlib>
extern "C" {
#include <dancer-xml.h>
#include <locale.h>
}

int main(const int ac, const char** av)
{
  dxml_element *xml, *member;
  const char* date;
  FILE*f;
  setlocale(LC_ALL, "ja_JP.UTF-8");

  if (ac == 1)
    {
      std::cerr << av[0]
		<< " [gijiroku.xml file names]"
		<< "\t lists the number of times a person has attended the meeting "
		<< std::endl;
      return 1;
    }

  for (int i=1; i<ac; i++)
    {
      f=fopen(av[i], "r");
      if (!f)
	{
	  perror("fopen");
	  return 1;
	}
      xml=dxml_read_xml(f);

      date=dxml_get_PCDATA_bysimplepath(xml, "meetinglog/head/date");
      member=dxml_get_element_bysimplepath(xml, "meetinglog/head/member");

      while(member)
	{
	  std::cout << member->child->element_name << ","
		    << date << "," << av[i] << std::endl;
	  member=member->next;
	}
      dxml_free_xml(xml);
      fclose (f);
    }
}
