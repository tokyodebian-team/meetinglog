#!/bin/bash
set -e

diff -u \
    <(xsltproc txt.xsl testdata/basic.xml) \
    <(./meeting.cc < testdata/basic.xml)

