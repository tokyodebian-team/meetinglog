// Partial interactive HTML file reloader.
// Will reload HTML file if modified.

var kSecMsecs = 1000
var timer_interval = 15 * kSecMsecs;

var my_timer = function() {
    var xml = new XMLHttpRequest();
    xml.open('GET', window.location + '#test', true);
    xml.onreadystatechange = function() {
	if (xml.readyState == 4) {
	    on_xml_loaded(xml);
	}
    };
    xml.overrideMimeType('text/xml');
    xml.send('');
    setTimeout(my_timer, timer_interval);
};

var counter = 0;


// output debug string to last-updated field.
var debug_output = function(s) {
    document.getElementById('lastupdated').innerHTML = s;
}

var on_xml_loaded = function(xml) {
    // get the XMLHTTPRequest response text into res value.
    var res=xml.responseText;
    debug_output("test" + counter);

    for (var i = 1; 
	 i < 20;
	 ++i) {
	var re_prev = new RegExp("<td id=\"previous"+i+"\">(.*)</td>");
	var re_discuss = new RegExp("<td id=\"discussed"+i+"\">(.*)</td>");
	var new_entry_prev;
	// update previousN entry
	new_entry_prev = res.match(re_prev);
	var doc_prev = document.getElementById("previous" + i);
	if (doc_prev && new_entry_prev && new_entry_prev[1]) {
	    doc_prev.innerHTML = new_entry_prev[1];
	}

	// update discussedN entry
	var new_entry_discuss;
	new_entry_discuss = res.match(re_discuss);
	var doc_discuss = document.getElementById("discussed" + i);
	if (doc_discuss && new_entry_discuss && new_entry_discuss[1]) {
	    doc_discuss.innerHTML = new_entry_discuss[1];
	}
    }
    counter++;
};

my_timer();
